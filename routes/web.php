<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(\App\Http\Controllers\PageController::class)
    ->name('page.')->group(function () {
        Route::get('/','index')->name('index');
        Route::get('/contact-us','contactUs')->name('contact');
        Route::get('/about-us','aboutUs')->name('about');
        Route::get('/sitemap.xml','sitemap')->name('sitemaps');
        Route::get('/sitemap.xml/articles','sitemap')->name('sitemap');
    });

Route::controller(\App\Http\Controllers\BlogController::class)
    ->name('blog.')->group(function () {
        Route::get('/articles','index')->name('articles');
        Route::get('/{slug}','single')->name('detail');
    });
