<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\PostController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\CommandController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('login', [AuthController::class , 'auth']);
Route::get('ping', [ApiController::class, 'ping']);

Route::middleware('auth:sanctum')
    ->withoutMiddleware('throttle:api')->group(function () {
    Route::post('check-token', [AuthController::class , 'checkToken']);
    Route::post('change-password', [AuthController::class , 'changePassword']);

    Route::prefix('posts')->group(function () {
        Route::get('/', [PostController::class, 'all']);
        Route::get('/{id}/show', [PostController::class, 'show']);
        Route::post('store', [PostController::class, 'store']);
        Route::put('{id}/update', [PostController::class, 'update']);
        Route::delete('{id}/destroy', [PostController::class, 'destroy']);
    });

    Route::prefix('comments')->group(function () {
        Route::get('/post/{post_id}/list', [CommentController::class, 'list']);
        Route::post('/post/{post_id}/store', [CommentController::class, 'store']);
        Route::get('/{id}/show', [CommentController::class, 'show']);
        Route::put('/{id}/update', [CommentController::class, 'update']);
        Route::delete('/{id}/destroy', [CommentController::class, 'destroy']);
    });

    Route::prefix('settings')->group(function () {
        Route::get('/', [SettingController::class, 'all']);
        Route::post('/update', [SettingController::class, 'update']);
        Route::post('/get', [SettingController::class, 'get']);
        Route::post('/set', [SettingController::class, 'set']);
        Route::delete('/delete', [SettingController::class, 'delete']);
    });

    Route::prefix('commands')->group(function () {
        Route::get('/cache', [CommandController::class, 'cache']);
        Route::get('/cache-clear', [CommandController::class, 'clearCache']);
        Route::get('/git-pull', [CommandController::class, 'gitPull']);
    });

});
