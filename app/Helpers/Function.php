<?php

if(! function_exists('setting')) {
    function setting($key , $default = null) {
        $setting = \App\Models\Setting::where('key', $key)->first();
        if($setting) {
            return $setting->value;
        }

        return $default;
    }
}

if(! function_exists('get_host_name')) {
    function get_host_name() {
        return str_replace('.','-', request()->getHttpHost());
    }
}

if(! function_exists('setting_set')) {
    function setting_set($key , $value) {
        $setting = \App\Models\Setting::where('key', $key)->first();
        if($setting) {
            $setting->value = $value;
            $setting->save();
            return $value;
        }

        \App\Models\Setting::insert([
            'key' => $key,
            'value' => $value
        ]);

        return $value;
    }
}
