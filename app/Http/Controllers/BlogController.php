<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BlogController extends Controller
{
    public function index()
    {
        $posts = Cache::remember(get_host_name() . '-page-articles' ,600, function () {
            return Post::where('is_active', true)->orderBy('created_at', 'DESC')->paginate(20);
        });
        return view('blog.index', compact('posts'));
    }

    public function single($slug)
    {
        if (strpos($slug, 'ID-') !== false) {
            $id = intval(str_replace('ID-', '', $slug));
            $post = Post::where('id', $id)->with('comments')->first();
            if($post) {
                return redirect(route('blog.detail', ['slug' => $post->slug]), 301);
            } else {
                $post = Post::where('slug', $slug)->with('comments')->firstOrFail();
            }
        } else {
            $post = Cache::remember(get_host_name() . "-{$slug}-post" ,600, function () use ($slug) {
                return Post::where('slug', $slug)->with('comments')->firstOrFail();
            });
        }


        $similar_articles = Cache::remember(get_host_name() . "-{$slug}-similar_articles" ,600, function () {
            return Post::select('title','slug','created_at')->inRandomOrder()->limit(10)->get();
        });

        $latest = Cache::remember(get_host_name() . "-{$slug}-latest" ,600, function () {
            return Post::select('title','slug','created_at')->orderBy('created_at', 'DESC')->limit(25)->get();
        });

        return view('blog.detail', compact('post', 'latest','similar_articles'));
    }
}
