<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    public function ping()
    {
        return response()->json([
            'status' => true,
            'message' => 'Site Is Up!',
        ], 200);
    }
}
