<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class CommandController extends Controller
{
    public function gitPull(Request $request)
    {
        Artisan::call('nano:git-pull --nano_version=' . $request->version ?? '1.0.0');

        return response()->json([
            'status' => true,
            'message' => 'Updated Git Repository Successfully',
        ], 200);
    }

    public function cache()
    {
        Artisan::call('route:cache');
        Artisan::call('view:cache');
        Artisan::call('config:cache');
        Artisan::call('event:cache');

        return response()->json([
            'status' => true,
            'message' => 'Start Laravel Cache Successfully',
        ], 200);
    }

    public function clearCache()
    {
        Artisan::call('optimize:clear');

        return response()->json([
            'status' => true,
            'message' => 'Optimize Clear Successfully',
        ], 200);
    }
}
