<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function auth(Request $request)
    {
        try {
            $validateUser = Validator::make($request->all(),
                [
                    'email' => 'required|email',
                    'password' => 'required'
                ]);

            if($validateUser->fails()){
                return response()->json([
                    'status' => false,
                    'message' => 'validation error',
                    'errors' => $validateUser->errors()
                ], 401);
            }

            if(!Auth::attempt($request->only(['email', 'password']))){
                return response()->json([
                    'status' => false,
                    'message' => 'Email & Password does not match with our record.',
                ], 401);
            }

            $user = User::where('email', $request->email)->first();

            return response()->json([
                'status' => true,
                'message' => 'User Logged In Successfully',
                'token' => $user->createToken("API TOKEN")->plainTextToken
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status' => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }

    public function checkToken(Request $request)
    {
        return response()->json([
            'status' => true,
            'message' => 'Token Is Successfully',
            'token' => $request->header('authorization')
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $validateUser = Validator::make($request->all(),
            [
                'old_password' => 'required',
                'new_password' => 'required',
            ]);

        if($validateUser->fails()){
            return response()->json([
                'status' => false,
                'message' => 'validation error',
                'errors' => $validateUser->errors()
            ], 401);
        }

        if(Hash::check($request->old_password,Auth::user()->password)) {
            Auth::user()->update([
                'password' => bcrypt($request->new_password),
                'updated_at' => Carbon::now()
            ]);
            return response()->json([
                'status' => true,
                'message' => 'Password Changed Successfully',
                'data' => $request->new_password
            ], 401);
        } else {
            return response()->json([
                'status' => false,
                'message' => 'Old Password not match',
            ], 401);
        }
    }
}
