<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function all(Request $request)
    {

        $per_page = 10;
        if($request->per_page) {
            $request->validate([
                'per_page' => 'numeric'
            ]);
            $per_page = $request->per_page;
        }

        return response()->json([
            'status' => true,
            'message' => 'Get Posts List Successfully',
            'data' => Post::orderBy('created_at','DESC')->paginate($per_page)
        ], 200);
    }

    public function show($id)
    {
        if(!Post::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Post Is Not Found!',
                'data' => []
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'Updated Post is Successfully',
            'data' => Post::where('id',$id)->with('comments')->first()
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'slug' => 'required|unique:posts',
            'author' => 'required',
            'body' => 'required',
        ]);

        $post_id = Post::insertGetId([
            'title' => $request->title,
            'slug'=> $request->slug,
            'author'=> $request->author,
            'content'=> $request->body,
            'thumbnail'=> $request->thumbnail ?? null,
            'categories'=> $request->categories ?? null,
            'tags'=> $request->tags ?? null,
            'is_active'=> $request->is_active ?? 1,
            'created_at' => $request->created_at ?? Carbon::now()
        ]);

        if($post_id) {
            return response()->json([
                'status' => true,
                'message' => 'Created New Post Successfully',
                'data' => Post::where('id',$post_id)->first()
            ], 200);
        }

        return response()->json([
            'status' => false,
            'message' => 'Error Insert New Post',
            'data' => []
        ], 500);

    }

    public function update($id, Request $request)
    {
        if(!Post::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Post Is Not Found!',
                'data' => []
            ], 404);
        }

        $request->validate([
            'title' => 'required',
            'slug' => 'required',
            'author' => 'required',
            'body' => 'required',
        ]);

        $post = Post::where('id', $id)->first();
        if($post->slug != $request->slug) {
            $request->validate([
                'slug' => 'required|unique:posts',
            ]);
        }

        Post::where('id', $id)->update([
            'title' => $request->title,
            'slug'=> $request->slug,
            'author'=> $request->author,
            'content'=> $request->body,
            'thumbnail'=> $request->thumbnail ?? null,
            'categories'=> $request->categories ?? null,
            'tags'=> $request->tags ?? null,
            'is_active'=> $request->is_active ?? 1,
            'updated_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Updated Post is Successfully',
            'data' => Post::where('id',$id)->first()
        ], 200);
    }

    public function destroy($id)
    {
        if(!Post::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Post Is Not Found!',
                'data' => []
            ], 404);
        }

        Post::where('id', $id)->delete();

        return response()->json([
            'status' => true,
            'message' => 'Deleted Post is Successfully'
        ], 200);
    }
}
