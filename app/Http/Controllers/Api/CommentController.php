<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function list($post_id, Request $request)
    {
        $per_page = 10;
        if($request->per_page) {
            $request->validate([
                'per_page' => 'numeric'
            ]);
            $per_page = $request->per_page;
        }

        $comments = Comment::where('post_id', $post_id)->orderBy('created_at', 'DESC')->paginate($per_page);

        return response()->json([
            'status' => true,
            'message' => 'Get Post Comments List Successfully',
            'data' => $comments
        ], 200);
    }

    public function store($post_id, Request $request)
    {
        if(!Post::where('id', $post_id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Post Is Not Found!',
                'data' => []
            ], 404);
        }

        $request->validate([
            'author' => 'required',
            'comment' => 'required'
        ]);

        $comment_id = Comment::insertGetId([
            'post_id' => $post_id,
            'author' => $request->author ?? null,
            'avatar' => $request->avatar ?? null,
            'comment' => $request->comment,
            'status' => $request->status ?? 'pending',
            'created_at' => $request->created_at ?? Carbon::now()
        ]);

        if($comment_id) {
            return response()->json([
                'status' => true,
                'message' => 'Created New Comment For Post Successfully',
                'data' => Comment::where('id',$comment_id)->with('post')->first()
            ], 200);
        }

        return response()->json([
            'status' => false,
            'message' => 'Error Insert New Comment',
            'data' => []
        ], 500);

    }

    public function show($id)
    {
        if(!Comment::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Comment Is Not Found!',
                'data' => []
            ], 404);
        }

        return response()->json([
            'status' => true,
            'message' => 'Get Comment Is Successfully',
            'data' => Comment::where('id',$id)->with('post')->first()
        ], 200);
    }

    public function update($id, Request $request)
    {
        if(!Comment::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Comment Is Not Found!',
                'data' => []
            ], 404);
        }

        $request->validate([
            'author' => 'required',
            'comment' => 'required'
        ]);

        Comment::where('id', $id)->update([
            'author' => $request->author ?? null,
            'avatar' => $request->avatar ?? null,
            'comment' => $request->comment,
            'status' => $request->status ?? 'pending',
            'updated_at' => Carbon::now()
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Updated Comment Is Successfully',
            'data' => Comment::where('id',$id)->with('post')->first()
        ], 200);
    }

    public function destroy($id)
    {
        if(!Comment::where('id', $id)->exists()) {
            return response()->json([
                'status' => false,
                'message' => 'Comment Is Not Found!',
                'data' => []
            ], 404);
        }

        Comment::where('id', $id)->delete();

        return response()->json([
            'status' => true,
            'message' => 'Deleted Comment is Successfully'
        ], 200);
    }
}
