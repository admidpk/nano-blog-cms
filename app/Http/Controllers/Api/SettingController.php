<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function all(Request $request)
    {
        $per_page = 10;
        if($request->per_page) {
            $request->validate([
                'per_page' => 'numeric'
            ]);
            $per_page = $request->per_page;
        }

        $settings = Setting::orderBy('id', 'ASC')->paginate($per_page);

        return response()->json([
            'status' => true,
            'message' => 'Get All Settings Successfully',
            'data' => $settings
        ], 200);
    }

    public function update(Request $request)
    {
        $request->validate(['settings' => 'required']);

        if(is_array($request->settings)) {
            foreach ($request->settings ?? [] as $key => $value) {
                if(Setting::where('key', $key)->exists()) {
                    Setting::where('key', $key)->update([
                        'value' => $value
                    ]);
                } else {
                    Setting::insert([
                        'key' => $key,
                        'value' => $value,
                    ]);
                }
            }
        }

        return response()->json([
            'status' => true,
            'message' => 'Settings Updated Is Successfully',
            'data' => Setting::whereIn('key', array_keys($request->settings) ?? [])->get()
        ], 200);
    }

    public function get(Request $request)
    {
        $request->validate(['key' => 'required']);

        return response()->json([
            'status' => true,
            'message' => 'Get Setting By Key Is Successfully',
            'data' => Setting::where('key', $request->key)->first()
        ], 200);

    }

    public function set(Request $request)
    {
        $request->validate([
            'key' => 'required',
            'value' => 'required'
        ]);

        if(Setting::where('key', $request->key)->exists()) {
            Setting::where('key', $request->key)->update([
                'value' => $request->value
            ]);
        } else {
            Setting::insert([
                'key' => $request->key,
                'value' => $request->value,
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Set Setting By Key And Value Is Successfully',
            'data' => Setting::where('key', $request->key)->first()
        ], 200);
    }

    public function delete(Request $request)
    {
        $request->validate(['key' => 'required']);

        Setting::where('key', $request->key)->delete();

        return response()->json([
            'status' => true,
            'message' => 'Delete Setting Is Successfully'
        ], 200);
    }
}
