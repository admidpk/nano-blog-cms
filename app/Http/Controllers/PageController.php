<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class PageController extends Controller
{
    public function index()
    {

        $posts = Cache::remember(get_host_name() . '-posts' ,600, function () {
            return Post::where('is_active', true)->orderBy('created_at', 'DESC')->limit(20)->get();
        });
        return view('index', compact('posts'));
    }

    public function aboutUs()
    {
        return view('about-us');
    }

    public function contactUs()
    {
        return view('contact-us');
    }

    public function sitemaps()
    {
        $posts = Post::select('slug','is_active','created_at')->where('is_active', true)->latest()->count();
        $lastPage = $posts / 1000;
        if(intval($lastPage) < $lastPage) {
            $page = intval($lastPage) + 1;
        } else {
            $page = intval($lastPage);
        }

        return response()->view('sitemaps', [
            'lastPage' => $page
        ])->header('Content-Type', 'text/xml');
    }

    public function sitemap()
    {
        $posts = Post::select('slug','is_active','created_at')->where('is_active', true)->latest()->paginate(5000);
        return response()->view('sitemap', [
            'posts' => $posts
        ])->header('Content-Type', 'text/xml');
    }
}
