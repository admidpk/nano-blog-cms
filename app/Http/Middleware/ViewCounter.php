<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Jenssegers\Agent\Agent;

class ViewCounter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $agent = new Agent();

//        dd($request->headers);
        $view = [
            "page" => request()->url(),
            "ip" => request()->ip(),
            "platform" => $agent->platform(),
            "browser" => $agent->browser(),
            "device" => $agent->device(),
            "robot" => $agent->robot(),
            "referral" => Cookie::get('referral'),
            "referer" => $request->headers->get('referer'),
            "viewed_at" => Carbon::now(),
        ];
        Log::info(json_encode($view));
        return $next($request);
    }
}
