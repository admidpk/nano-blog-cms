<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Process;

class GitPullCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nano:git-pull {--nano_version=1.0.0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for update template with git repository';

    private $pullLog = [];

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $path = base_path('/');
        Process::path($path)->timeout(120)->run("git config --global --add safe.directory $path", function($type, $buffer) {
            $this->pullLog[] = $buffer;
        });
        Process::path($path)->timeout(120)
            ->run('git pull origin master',
                function($type, $buffer) {
            $this->pullLog[] = $buffer;

            if($buffer == "Already up to date.\n") {
                $this->alreadyUpToDate = TRUE;
                setting_set('nano_version', $this->option('nano_version'));
                setting_set('nano_updated_at', Carbon::now());

                $path = base_path('/');
                Process::path($path)->timeout(120)
                    ->run('composer install', function ($type, $buffer) {
                    $this->pullLog[] = $buffer;
                });
            }
        });

        Log::info((json_encode($this->pullLog)));
        $this->info(json_encode($this->pullLog));

    }
}
