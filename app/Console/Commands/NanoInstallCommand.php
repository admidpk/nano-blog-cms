<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class NanoInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nano:install {--email=info@example.com} {--password=123456789}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command For Nano Install';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        \App\Models\User::factory()->create([
            'name' => 'Administrator',
            'email' => $this->option('email'),
            'password' => bcrypt($this->option('password')),
        ]);

        $this->info('Created Administrator user!');
    }
}
