@extends('layouts.master')
@section('title', 'صفحه اصلی')
@section('description', setting('site_description', 'توضیحات سایت نانو بلاگ'))
@section('content')
    @php
        $author = setting('site_author', 'مدیر سایت');
    @endphp
    @include('layouts.header', [
        'title' => setting('site_name', 'نانو بلاگ'),
        'description' => setting('site_description', 'توضیحات سایت نانو بلاگ'),
        'background' => 'assets/img/home-bg.jpg',
    ])
    <!-- Main Content-->
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                @foreach($posts as $post)
                <!-- Post preview-->
                <div class="post-preview">
                    <a href="{{route('blog.detail',['slug' => $post->slug])}}">
                        <h2 class="post-title">{{$post->title}}</h2>
                        <h3 class="post-subtitle">{{$post->short()}}</h3>
                    </a>
                    <p class="post-meta">
                        توسط
                        <a href="{{url('/')}}">{{$post->author ?? $author}}</a>
                        در {{jdate($post->created_at)->format('j F Y')}}
                    </p>
                </div>
                <!-- Divider-->
                <hr class="my-4" />
                @endforeach
                <!-- Pager-->
                <div class="d-flex justify-content-end mb-4"><a class="btn btn-primary text-uppercase" href="{{route('blog.articles')}}">نمایش بیشتر →</a></div>
            </div>
        </div>
    </div>
@endsection
