@extends('layouts.master')
@section('title', 'درباره ما')
@section('description', '')
@section('content')
    @include('layouts.header', [
        'title' => 'درباره ما',
        'description' => 'هر چیزی که لازم است درباره ما بدانید',
        'background' => 'assets/img/about-bg.jpg',
    ])

    <main class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    {!! setting('about_us', 'توضیحات درباره ما') !!}
                </div>
            </div>
        </div>
    </main>
@endsection
