@extends('layouts.master')
@section('title', $post->title)
@section('description', $post->short())
@section('content')
    <header class="masthead" style="background-image: url('assets/img/home-bg.jpg')">
        <div class="container position-relative px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-10 col-lg-8 col-xl-7">
                    <div class="post-heading">
                        <h1>{{$post->title}}</h1>
                        <h2 class="subheading">{{$post->short()}}</h2>
                        <span class="meta">
                                توسط
                        <a href="{{url('/')}}">{{$post->author ?? $author}}</a>
                        در {{jdate($post->created_at)->format('j F Y')}}
                            </span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <article class="mb-4">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-md-8 col-lg-8 col-xl-8">
                    <div class="box-main-content">
                        {!! setting('article_top_content') !!}

                        {!! $post->content !!}

                        {!! setting('article_bottom_content') !!}
                    </div>

                </div>
                <div class="col-md-4 col-lg-4 col-xl-4">

                    {!! setting('article_top_sidebar') !!}

                    <div class="box-sidebar">
                        <div class="box-sidebar-header">
                            <h5>آخرین مطالب</h5>
                        </div>
                        <div class="box-sidebar-body">
                            <ul>
                                @foreach($latest as $item)
                                    <li>
                                        <a href="{{url($item->slug)}}" title="{{$item->title}}">{{$item->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div id="sh-scripts-c110" style="margin: 15px 0"></div>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js" integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
                    <script src="https://panel.seohacker.academy/assets/scripts/random-titles.js"></script>

                    {!! setting('article_bottom_sidebar') !!}

                </div>
                <div class="col-md-8 col-lg-8 col-xl-8">
                    <h5>مقالات مشابه</h5>
                    <div class="card mb-4">
                        <div class="card-body">
                            <ul class="mt-3">
                                @foreach($similar_articles as $article)
                                    <li class="mb-2">
                                        <a href="{{url($article->slug)}}" title="{{$article->title}}">{{$article->title}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <h5>نظرات کاربرن</h5>

                    <div class="card">
                        <div class="card-body">
                            @foreach($post->comments as $comment)
                                <div class="d-flex flex-end align-items-center">
                                    <img class="rounded-circle shadow-1-strong ms-2" src="{{asset('assets/img/profile.png')}}" alt="avatar" width="60" height="60">
                                    <div>
                                        <h6 class="fw-bold text-primary mb-1">{{$comment->author}}</h6>
                                        <span class="text-muted small mb-0">
                                        تاریخ ثبت : {{jdate($comment->created_at)->format('j F Y')}}
                                    </span>
                                    </div>
                                </div>

                                <p class="mt-3 mb-4 pb-2">
                                    {!! $comment->comment !!}
                                </p>
                                @if(!$loop->last)
                                    <hr>
                                @endif
                            @endforeach
                        </div>
                        <div class="card-footer py-3 border-0" style="background-color: #f8f9fa;">
                            <div class="d-flex flex-start w-100">
                                <div class="form-outline w-100">
                                    <textarea class="form-control" id="textAreaExample" rows="4" style="background: #fff;"></textarea>
                                    <label class="form-label" for="textAreaExample" style="margin-left: 0px;"></label>
                                    <div class="form-notch"><div class="form-notch-leading" style="width: 9px;"></div><div class="form-notch-middle" style="width: 60px;"></div><div class="form-notch-trailing"></div></div></div>
                            </div>
                            <div class="float-end mt-0 pt-1">
                                <button type="button" class="btn btn-primary btn-sm">ثبت نظر</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-xl-4"></div>
            </div>
        </div>
    </article>
@endsection
