@extends('layouts.master')
@section('title', 'لیست مقالات')
@section('description', 'توضیحات صفحه لیست مفالات')
@section('content')
    @include('layouts.header', [
        'title' => 'مقالات',
        'description' => 'لیست کامل مقالات منتشر شده',
        'background' => 'assets/img/home-bg.jpg',
    ])
    <!-- Main Content-->
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                @foreach($posts as $post)
                    <!-- Post preview-->
                    <div class="post-preview">
                        <a href="{{route('blog.detail',['slug' => $post->slug])}}">
                            <h2 class="post-title">{{$post->title}}</h2>
                            <h3 class="post-subtitle">{{$post->short()}}</h3>
                        </a>
                        <p class="post-meta">
                            توسط
                            <a href="{{url('/')}}">{{$post->author ?? $author}}</a>
                            در {{jdate($post->created_at)->format('j F Y')}}
                        </p>
                    </div>
                    <!-- Divider-->
                    <hr class="my-4" />
                @endforeach

                <!-- Pager-->
                    {!! $posts->links() !!}
            </div>
        </div>
    </div>
@endsection
