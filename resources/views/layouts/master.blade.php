<!DOCTYPE html>
<html lang="fa">
<head>
    <!-- Test GIT 3 -->
    <meta charset="utf-8" />
    {!! setting('site_google_verification') !!}
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="@yield('description')" />
    <meta name="author" content="{{setting('site_author', 'مدیر سایت')}}" />
    <title>@yield('title') - {{setting('site_name', 'نانو بلاگ')}}</title>
    <link rel="icon" type="image/x-icon" href="{{asset('assets/favicon.ico')}}" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{asset('css/styles.css')}}" rel="stylesheet" />
    <link href="{{asset('css/custom.css')}}?v=1.0.3" rel="stylesheet" />

    @yield('styles')
    <style>
        a {
            color: #0d6efd;
        }
        ul.pagination {
            flex-wrap: wrap !important;
        }
        .box-main-content {
            width: 100%;
            border: 1px solid #dcdcdc;
            padding: 0 20px;
            text-align: justify;
            margin-bottom: 30px;
        }
        .box-main-content img {
            max-width: 100%;
        }
        .box-sidebar {
            width: 100%;
            border: 1px solid #dcdcdc;
        }
        .box-sidebar-header {
            width: 100%;
            padding: 8px 10px;
            border-bottom: 1px solid #dcdcdc;
            background: #f1f1f1;
        }
        .box-sidebar-body {
            padding: 15px;
        }
        .box-sidebar-body ul {
            padding: 0;
        }
        .box-sidebar-body ul li {
            list-style: none;
            margin-bottom: 10px;
            border-bottom: 1px dashed silver;
            padding-bottom: 10px;
        }
        .box-sidebar-body ul li a {
            font-size: 17px;
        }
        .box-sidebar-footer {}
    </style>
    {!! setting('site_head_script') !!}

</head>
<body>
<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{route('page.index')}}">{{setting('site_name_en', 'NanoBlog')}}</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            منوی اصلی
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ms-auto py-4 py-lg-0">
                <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="{{route('page.index')}}">صفحه اصلی</a></li>
                <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="{{route('blog.articles')}}">مقالات</a></li>
                <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="{{route('page.about')}}">درباره ما</a></li>
                <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="{{route('page.contact')}}">تماس با ما</a></li>
                <li class="nav-item"><a class="nav-link px-lg-3 py-3 py-lg-4" href="{{url('sitemap.xml')}}" target="_blank">نقشه سایت</a></li>
            </ul>
        </div>
    </div>
</nav>
@yield('content')
<!-- Footer-->
<footer class="border-top">
    <div class="container px-4 px-lg-5">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-md-10 col-lg-8 col-xl-7">
                <ul class="list-inline text-center p-0">
                    <li class="list-inline-item">
                        <a href="{{setting('social_twitter', '#')}}">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{setting('social_facebook', '#')}}">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{setting('social_github', '#')}}">
                            <span class="fa-stack fa-lg">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                            </span>
                        </a>
                    </li>
                </ul>
                <div class="small text-center text-muted fst-italic">Copyright &copy;{{date('Y')}} Power By: {{setting('site_name_en', 'NanoBlog')}}</div>
            </div>
        </div>
    </div>
</footer>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<script src="{{asset('js/scripts.js')}}"></script>
{!! setting('site_footer_script') !!}
@yield('scripts')
</body>
</html>
